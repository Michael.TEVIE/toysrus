<?php

namespace App;

use App\Controllers\AuthController;
use App\Controllers\PageController;
use App\Controllers\ToyController;
use App\Model\Toy;
use Core\Database\DatabaseConfigInterface;
use MiladRahimi\PhpRouter\Exceptions\InvalidCallableException;
use MiladRahimi\PhpRouter\Exceptions\RouteNotFoundException;
use MiladRahimi\PhpRouter\Router;

class App implements DatabaseConfigInterface
{
    // On déclare les constantes
    private const DB_HOST = 'database';
    private const DB_NAME = 'lamp';
    private const DB_USER = 'lamp';
    private const DB_PASS = 'lamp';
    private static ?self $instance = null;

    // Méthode appelée au démarrage de l'appli (dans index.php)
    public static function getApp(): self
    {
        if(is_null(self::$instance)) self::$instance = new self();
        return self::$instance;
    }
    // On gère la partie router
    private Router $router;
    public function getRouter(): Router
    {
        return $this->router;
    }

    private function __construct()
    {
        $this->router = Router::create();
    }
    // On a 3 méthodes à déclarer
    // 1:  la méthode start: activation du router
    public function start()
    {
        session_start();
        $this->registerRoutes();
        $this->startRouter();
    }
    // 2: Méthode qui enregistre les routes
    public function registerRoutes()
    {
        // Déclaration des patterns pour tester les valeurs des arguments
        $this->router->pattern('id', '[1-9]\d*');
        $this->router->pattern('slug', '(\d+-)?[a-z]+(-[a-z\d]+)*');


        $this->router->get('/', [ToyController::class, 'index']);
        $this->router->get('/jouet/{id}', [ToyController::class, 'toyById']);
        // On enregistre la route de connexion
        $this->router->get('/connexion', [AuthController::class, 'index']);
        $this->router->post('/login', [AuthController::class, 'login']);
        }
    // 3: Méthode qui démarre le router
    public function startRouter()
    {
        try {
            $this->router->dispatch();
        }catch(RouteNotFoundException|InvalidCallableException $e){
            echo $e;
        }
    }

    public function getHost(): string
    {
        return self::DB_HOST;
    }

    public function getName(): string
    {
        return self::DB_NAME;
    }

    public function getUser(): string
    {
        return self::DB_USER;
    }

    public function getPass(): string
    {
        return self::DB_PASS;
    }
}
