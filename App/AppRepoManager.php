<?php

namespace App;

use App\Model\Repository\ToyRepository;
use App\Model\Repository\UserRepository;
use Core\RepositoryManagerTrait;


class AppRepoManager
{
    // On utilise le trait
    use RepositoryManagerTrait;

    private  UserRepository $userRepository;

    public function getUserRepo(): UserRepository
    {
        return $this ->userRepository;
    }


    private  ToyRepository $toyRepository;
    public function getToyRepo(): ToyRepository
    {
        return $this ->toyRepository;
    }

    public function __construct()
    {
        $config = App::getApp();
        $this->userRepository = new UserRepository($config);
        $this->toyRepository = new ToyRepository($config);
    }

}