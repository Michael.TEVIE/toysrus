<?php

namespace App\Controllers;

use App\AppRepoManager;
use Core\View;

class ToyController extends Controller
{
    public function index(): void
    {
        $view_data = [
            'title_tag' => 'Mon site',
            'h1_tag' => 'Nos jouets',
            'toys' => AppRepoManager::getRm()->getToyRepo()->findAll()
        ];

        $view = new View('toy/list');
        $view->titre = 'Tous nos jouets';
        $view->render($view_data);
    }
}