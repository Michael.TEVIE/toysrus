<?php

namespace App\Controllers;

use Core\View;

class PageController
{
    public function index()
    {
        // On met des données en dur
        $view_data = [
            'title_tag' => 'mon site',
            'list_title' => 'Bienvenue',
            'toy_list' =>  [
                'jouet 1',
                'jouet 2'
        ]
        ];
        $view = new View('page/home');
        $view->titre = 'Mon site MVC';
        $view->render($view_data);
    }

}