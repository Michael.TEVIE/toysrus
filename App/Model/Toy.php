<?php

namespace App\Model;

use Core\Model;

class Toy extends Model

{
    public string $name;
    public string $description;
    public int $brand;
    public float $price;
    public string $image;
    public string $slug;
}
