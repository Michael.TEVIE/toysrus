<?php

namespace App\Model;

use Core\Model;
class Store extends Model
{
    public string $name;
    public int $postal_code;
    public string $city;
}