<?php

namespace App\Model;

use Core\Model;

class User extends Model
{
    public const ROLES_SUBSCRIBER = 1;
    public const ROLES_ADMINISTRATOR = 2;

    public string $email;
    public string $password;
    public int $role;
}