<?php

namespace App\Model\Repository;

use App\Model\User;
use Core\Repository;

class UserRepository extends Repository
    {
        public function getTableName(): string
        {
            return 'users';
        }

        public function checkAuth(string $email, string $password): ?User
        {
            $q = sprintf(
                'SELECT * FROM `%s` WHERE `email`=:email AND `password`=:password',
                $this->getTableName()
            );

            $stmt = $this ->pdo->prepare($q);

            if(!$stmt) return null;

            $stmt->execute(['email' => $email, 'password' => $password]);

            $user_data = $stmt->fetch();

            return empty($user_data) ? null : new User($user_data);
        }
}