<?php

namespace App\Model\Repository;

use App\Model\Toy;
use Core\Repository;

class ToyRepository extends Repository
{
    public function getTableName(): string
    {
        return 'toys';
    }

    public function findAll(): array
    {
        return $this->readAll(Toy::class);
    }
}