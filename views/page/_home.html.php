<main>
    <H1><?PHP echo $list_title ?></H1>
    <?php if(empty($toy_list)): ?>
        <div>Aucun jouet en ce moment</div>
    <?php else: ?>
        <ul>
            <?php foreach ($toy_list as $toy): ?>
                <li><?php echo $toy ?></li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</main>

