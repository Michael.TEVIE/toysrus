<h1><?PHP echo $h1_tag ?></h1>
<?php if(empty($toys)): ?>

    <div>Aucun jouet à afficher</div>
<?php else: ?>
    <div class="d-flex flex-row flex-wrap justify-content-center col-6">
        <?php foreach ($toys as $toy): ?>
            <div class="card m-2" style="width: 22rem">
                <img src="/img/<?php echo $toy->image; ?>"
                class="card-img-top img-fluid p-3"
                alt="<?php echo $toy->name ?>">
                <div class="card-body">
                    <h3 class="card-title"><?php echo $toy->name ?></h3>
                    <p class="card-text"><?php echo $toy->price ?>€</p>
                    <!-- a href="#" class="btn btn-primary">Voir détail</a -->
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#<?php echo $toy->slug ?>Modal">
                        voir le détail
                    </button>

                    <!-- Modal prise de boostrap -->
                    <div class="modal fade" id="<?php echo $toy->slug ?>Modal" tabindex="-1" aria-labelledby="<?php echo $toy->slug ?>ModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h1 class="modal-title fs-5" id="<?php echo $toy->slug ?>ModalLabel"><?php echo $toy->name ?></h1>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <p><?php echo $toy->description ?></p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

