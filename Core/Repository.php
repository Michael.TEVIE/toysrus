<?php

namespace Core;

use Core\Database\Database;
use Core\Database\DatabaseConfigInterface;

abstract class Repository
{

    protected \PDO $pdo;

    abstract public function getTableName(): string;

    public function __construct(DatabaseConfigInterface $config)
    {
        $this->pdo = Database::getPDO($config);
    }

    protected function readAll(string $class_name): array
    {
        $arr_result = [];
        $q = sprintf('SELECT * FROM `%s`', $this->getTableName());
        $stmt = $this->pdo->query($q);

        if(!$stmt) return $arr_result;
        while($row_data = $stmt->fetch()) $arr_result[] = new $class_name($row_data);
        return $arr_result;
    }
}